#!/bin/sh
# \
exec tclsh $0 $*

source srcs.tcl

lassign $argv path
if {$path == ""} {
	error "use: <path>"
}

set conf [srcs::load_tms $path]
set n [dict get $conf n]

srcs::plan_new .E $conf
srcs::c_matr_new .E.o 1 $n
srcs::c_plan_order .E.o .E

# fake graph = transitive closure
srcs::c_plan_new .H $n
srcs::c_plan_tc .H .E .E.o

# decide which tasks are uncertain
set upct 0.8
set Tu [list]
set Tc [list]

for {set j 0} {$j < $n} {incr j} {
	if {[math::random] <= [expr 1.0 - $upct] || $j == 0 || $j == [expr $n-1]} {
		lappend Tc $j
	} else {
		lappend Tu $j
	}
}

# no incomming arcs for certain tasks
foreach j $Tc {
	for {set i 0} {$i < $n} {incr i} {
		srcs::c_plan_set_edge .H $i $j 0
	}
}

# no incomming arcs from uncertain tasks
foreach i $Tu {
	for {set j 0} {$j < $n} {incr j} {
		srcs::c_plan_set_edge .H $i $j 0
	}
}

# no incomming arcs from 0 or 1
for {set j 0} {$j < $n} {incr j} {
	srcs::c_plan_set_edge .H 0 $j 0
	srcs::c_plan_set_edge .H 1 $j 0
}

# redefine Tu
set Tu' [list]
foreach j $Tu {
	set haspre 0
	for {set i 0} {$i < $n} {incr i} {
		if [srcs::c_plan_get_edge .H $i $j] {
			set haspre 1 
			break
		}
	}
	if {$haspre} {
		lappend Tu' $j
	} else {
		lappend Tc $j
	}
}
set Tu ${Tu'}

# keep <= vmax of incomming arcs for uncertain tasks
set vmax 2
set H [dict create Tc $Tc Tu $Tu]
foreach j $Tu {
	dict set H pre:$j [list]
	set delete 0
	for {set i [expr $n-1]} {$i >= 0} {incr i -1} {
		if {$delete} {
			srcs::c_plan_set_edge .H $i $j 0
			continue
		}
		if {![srcs::c_plan_get_edge .H $i $j]} {
			continue
		}
		dict lappend H pre:$j $i

		if {[llength [dict get $H pre:$j]] >= $vmax} {
			set delete 1
			# delete all remaining arcs in .H from this point on
		}
	}
}

# color arcs in E
for {set i 0} {$i < $n} {incr i} {
	for {set j 0} {$j < $n} {incr j} {
		if [srcs::c_plan_get_edge .H $i $j] {
			srcs::c_plan_set_edge .E $i $j 2
		}
	}
}

puts $H
#srcs::plan_print_dot .E $conf
