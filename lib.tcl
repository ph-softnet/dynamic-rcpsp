
proc sol_alloc {sol conf {simmax 1}} {
	set n [dict get $conf n]
	srcs::c_plan_new $sol $n
	srcs::c_matr_new $sol.order 1 $n
	srcs::c_matr_new $sol.est $simmax $n
	srcs::c_matr_new $sol.lft $simmax $n
	srcs::c_matr_new $sol.eseq 1 $n
}

proc sol_cutwalk_sgs {sol plan d q b conf {aux ""}} {
	set aux [dict create {*}$aux]

	if [dict exists $aux simnum] {
		set simnum [dict get $aux simnum]
	} else {
		set simnum 1
	}

	# .sol.lft
	srcs::c_mcarlo_lft $sol.lft $plan $plan.order $d 9999 $simnum

	# .sol.eseq
	if [dict exists $aux eseq] {
		set eseq [dict get $aux eseq]
	} else {
		set lfts [srcs::matr_get_row $sol.lft 0 real]
		set rule [srcs::sortlist $lfts indices]
		set eseq [srcs::sgs::eligseq $conf $rule .plan]
	}
	srcs::matr_set_row $sol.eseq 0 $eseq int

	# .sol (serial sgs)
	srcs::c_plan_copy $sol $plan
	srcs::c_sgs_cutwalk1 $sol $plan $d $simnum $q $b $sol.eseq 1000 TRQ
}

proc sol_serial_sgs {sol plan d q b conf {aux ""}} {
	set aux [dict create {*}$aux]

	if [dict exists $aux simnum] {
		set simnum [dict get $aux simnum]
	} else {
		set simnum 1
	}

	# .sol.lft
	srcs::c_mcarlo_lft $sol.lft $plan $plan.order $d 9999 $simnum

	# .sol.eseq
	if [dict exists $aux eseq] {
		set eseq [dict get $aux eseq]
	} else {
		set lfts [srcs::matr_get_row $sol.lft 0 real]
		set rule [srcs::sortlist $lfts indices]
		set eseq [srcs::sgs::eligseq $conf $rule .plan]
	}
	srcs::matr_set_row $sol.eseq 0 $eseq int

	# .sol (serial sgs)
	srcs::c_plan_copy $sol $plan
	srcs::c_sgs_cutwalk0 $sol $plan $d $simnum $q $b $sol.eseq 0
}

proc sol_rho {sol plan d q b conf {aux ""}} {
	set aux [dict create {*}$aux]

	if [dict exists $aux simnum] {
		set simnum [dict get $aux simnum]
	} else {
		set simnum 1
	}

	# .sol.lft
	srcs::c_mcarlo_lft $sol.lft $plan $plan.order $d 9999 $simnum

	# .sol.eseq
	if [dict exists $aux eseq] {
		set eseq [dict get $aux eseq]
	} else {
		set lfts [srcs::matr_get_row $sol.lft 0 real]
		set rule [srcs::sortlist $lfts indices]
		set eseq [srcs::sgs::eligseq $conf $rule .plan]
	}
	srcs::matr_set_row $sol.eseq 0 $eseq int

	# .sol (rho sgs)
	srcs::c_plan_copy $sol $plan
	if [dict exists $aux part] {
		set part [dict get $aux part]
	} else {
		set part [dict get $conf n]
	}

	srcs::c_rho_part $sol $plan $plan.order $plan.tc $d $simnum $q $b $sol.eseq $part
}

proc sol_run {est sol d {simnum 1}} {
	if { [srcs::c_plan_order $sol.order $sol] < 0 } {
		error "cycle in $sol"
	}
	srcs::c_mcarlo_est $est $sol $sol.order $d .zero $simnum
	return [srcs::matr_get_row $est 0 real]
}

proc sol_fbi {sol plan bplan d q b conf bconf} {
	sol_serial_sgs $sol $plan $d $q $b $conf

	set s0 [sol_run $sol.est $sol $d]
	set r0 [srcs::fbi::desc_lft_sort [srcs::matr_get_row $d 0 real] $s0]
	set e0 [srcs::sgs::eligseq $bconf $r0 $bplan]

	# backward s1: serial SGS + decreasing LFT in s0
	sol_serial_sgs $sol $bplan $d $q $b $bconf [list eseq $e0]
	set s1 [sol_run $sol.est $sol $d]
	set r1 [srcs::fbi::desc_lft_sort [srcs::matr_get_row $d 0 real] $s1]
	set e1 [srcs::sgs::eligseq $conf $r1 $plan]

	# forward s2: serial SGS + decreasing LFT in s1
	sol_serial_sgs $sol $plan $d $q $b $conf [list eseq $e1]
	set s2 [sol_run $sol.est $sol $d]

	return [list $s0 $s2]
}

proc loaddict {path} {
	set f [open $path r]
	set d [dict create {*}[read $f]]
	close $f
	return $d
}

proc srcs::c_trq_weights {w d q simnum} {
	set n [srcs::c_matr_get_cols $d]
	set k [srcs::c_matr_get_cols $q]

	for {set i 0} {$i < $n} {incr i} {
		set Qi 0
		for {set r 0} {$r < $k} {incr r} {
			set qir [srcs::c_matr_get .q $i $r real]
			set Qi [expr $Qi + $qir]
		}

		set wi 0
		for {set sim 0} {$sim < $simnum} {incr sim} {
			set di [srcs::c_matr_get .d $sim $i real]
			set wi [expr $Qi * $di]
			srcs::c_matr_set $w $sim $i real $wi
		}
	}
}

proc srcs::c_trq {trq est w d simnum {hrzn 1000}} {
	set n [srcs::c_matr_get_cols $est]
	set T 0
	for {set i 0} {$i < $n} {incr i} {
		for {set sim 0} {$sim < $simnum} {incr sim} {
			set wi [srcs::c_matr_get $w $sim $i real]

			set si [srcs::c_matr_get $est $sim $i real]
			set di [srcs::c_matr_get $d $sim $i real]
			set Ti [expr $wi*($hrzn - ($si + 0.5*$di))]

			if {$trq != ".null"} {
				srcs::c_matr_set $trq $sim $i real $Ti
			}

			set T [expr $T + $Ti]
		}
	}
	return [set T [expr $T / $simnum]]
}

