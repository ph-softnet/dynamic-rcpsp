proc colorgradient {n c1 c2} {

      # Color intensities are from 0 to 65535, 2 byte colors.
      foreach {r1 g1 b1} [winfo rgb . $c1] break
      foreach {r2 g2 b2} [winfo rgb . $c2] break

      #puts "c1: $r1 $g1 $b1"
      #puts "c2: $r2 $g2 $b2"

      # Normalize intensities to 0 to 255, 1 byte colors.
      foreach el {r1 g1 b1 r2 g2 b2} {
        set $el [expr {[set $el] * 255 / 65535}].0
      }

      #puts "c1: $r1 $g1 $b1"
      #puts "c2: $r2 $g2 $b2"

      if {$n == 1} {
        set r_step 0.0 ; set g_step 0.0 ; set b_step 0.0
      } else {
        set r_step [expr {($r2-$r1) / ($n-1)}]
        set g_step [expr {($g2-$g1) / ($n-1)}]
        set b_step [expr {($b2-$b1) / ($n-1)}]
      }

      #puts "$r_step $g_step $b_step"

      set steps {}
      for {set i 0} {$i < $n} {incr i} {
        set r [expr {int($r_step * $i + $r1)}]
        set g [expr {int($g_step * $i + $g1)}]
        set b [expr {int($b_step * $i + $b1)}]
        #puts "$r $g $b"
        lappend steps [format "#%.2X%.2X%.2X" $r $g $b]
      }

      return $steps
}