#!/bin/sh
# \
exec tclsh $0 $*

# -- lib.tcl
# Library of useful functions

proc putfbi {sol d sgs {s0 ""}} {
	global n conf bconf
	
	# (1) get schedule
	if {$s0 == ""} {
		runsol $sol .d
		set s0 [srcs::matr_get_row $sol.est 0 real]
	}

	# (2) extract priority rule & eligibility sequence
	set r0 [srcs::fbi::desc_lft_sort [srcs::matr_get_row .d 0 real] $s0]
	set e0 [srcs::sgs::eligseq $bconf $r0 .bplan]

	# (3) make reverse schedule
	newsol $sol.bwd
	putsol $sol.bwd .d $sgs $e0 BWD
	runsol $sol.bwd .d
	set s1 [srcs::matr_get_row $sol.bwd.est 0 real]

	# (4) extract priority rule & eligibility sequence
	set r1 [srcs::fbi::desc_lft_sort [srcs::matr_get_row .d 0 real] $s1]
	set e1 [srcs::sgs::eligseq $conf $r1 .plan]

	# (5) make forward schedule by this eligibility sequence
	newsol $sol.fwd
	putsol $sol.fwd .d $sgs $e1
	runsol $sol.fwd .d
	set s2 [srcs::matr_get_row $sol.fwd.est 0 real]
	
	return [lindex $s2 [expr $n-1]]
}


# -- initialize a solution allocated with newsol
proc putsol {sol d mode {eseq ""} {bwd ""} {dt 0} {simnum 1}} {
	global conf bconf n
	
	set thisplan .plan
	set thisconf $conf
	if {$bwd == "BWD"} {
		set thisplan .bplan
		set thisconf $bconf
 	}

	if {$eseq == ""} {
 		srcs::c_mcarlo_lft $sol.lft $thisplan $thisplan.order $d 4321 1
		set lfts [srcs::matr_get_row $sol.lft 0 real]
 	
		set rule [srcs::sortlist $lfts indices]
		set eseq [srcs::sgs::eligseq $thisconf $rule .plan]
	}
	
 	srcs::matr_set_row $sol.eseq 0 $eseq int
	srcs::c_plan_copy $sol $thisplan
	
	if {$mode == "RHO"} {
		srcs::c_sgs_cutwalk_rho $sol $thisplan $d $simnum .q .b $sol.eseq
	}
	if {$mode == "SER"} {
		srcs::c_sgs_cutwalk0 $sol $thisplan $d $simnum .q .b $sol.eseq $dt
	}
	if {$mode == "TRQ" || $mode == "CMX" || $mode == "TRQ_CMX"} {
		srcs::c_sgs_cutwalk1 $sol $thisplan $d $simnum .q .b $sol.eseq [expr 10e3] $mode
	}
	
 	if { [srcs::c_plan_order $sol.order $sol] == -1 } {
			error "$sol cyclic"
	}
	
	return $eseq
}

# -- allocate a solution
proc newsol {sol {simnum 1}} {
	global n
	
	srcs::c_plan_new $sol $n
	srcs::c_matr_new $sol.order 1 $n
	srcs::c_matr_new $sol.eseq 1 $n
	srcs::c_matr_new $sol.lft 1 $n
	srcs::c_matr_new $sol.est $simnum $n
	srcs::c_matr_new $sol.d 1 $n
}

# -- run a solution after putsol
proc runsol {sol d {simnum 1} {P -1}} {
	global conf simmax n
	
	srcs::c_mcarlo_est $sol.est $sol $sol.order $d $simnum
	set mean [srcs::c_matr_m1 $sol.est $simnum [expr $n-1] real]
	if {$simnum > 1 && $P >= 0 && $P <= 1} {
		set rbst [srcs::c_matr_p_j $sol.est $simnum [expr $n-1] $P]
		set answ [list $mean $rbst]
	} else {
		set answ $mean
	}
	
	return $answ
}
