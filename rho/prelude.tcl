source rcpsp/fbi.tcl

# parse command line

set argc [llength $argv]
if {$argc < 1} {
	error "use: <path> \[aux ..\]"
}

set path [lindex $argv 0]
set aux [lrange $argv 1 end]

set simnum 1
set simmax 1


	
# load problem instance

set conf [srcs::load_tms $path]
set n [dict get $conf n]
set k [dict get $conf k]
incr k -1
# precedence constraints

srcs::plan_new .plan $conf
srcs::c_matr_new .plan.order 1 $n
if { [srcs::c_plan_order .plan.order .plan] == -1 } {
	error ".plan cyclic"
}

srcs::c_plan_new .plan.tc $n
srcs::c_plan_tc .plan.tc .plan .plan.order 

# initialize
set bconf [srcs::fbi::reverse_conf $conf .plan]
srcs::plan_new .bplan $bconf
srcs::c_matr_new .bplan.order 1 $n
if { [srcs::c_plan_order .bplan.order .bplan] == -1 } {
	error ".bplan cyclic"
}


# resource constraints

srcs::c_matr_new .q $n $k
srcs::c_matr_new .b 1 $k
srcs::qb_new .q .b $conf

# task durations

srcs::c_matr_new .d $simmax $n
srcs::uni_d_new .d $simmax $conf high

srcs::c_matr_new .d.mean 1 $n
for {set i 0} {$i < $n} {incr i} {
	srcs::c_matr_set .d.mean 0 $i real [set x [srcs::c_matr_m1 .d $simmax $i real]]
	srcs::c_matr_set .d 0 $i real $x
}

