#!/bin/sh
# \
exec wish $0 $* 

source s-rcpsp/rho/randomcolor.tcl
source s-rcpsp/rho/colorgradient.tcl

# canvas background
set COLOR0 "snow"

# seen and done
set COLOR1 "skyblue"

# unseen
set COLOR2 "grey"

# unseen but successor
set COLOR3 "#BA160C"

# inserted task
set COLOR4 "#FF4F00"

# -- chaining
proc chaining {s peak} {
	global n k
	set chn [dict create]
	
	for {set r 0} {$r < $k} {incr r} {
		set br [srcs::c_matr_get .b 0 $r int]
		set maxuse [dict get $peak $r]
		set br $maxuse
	
		for {set u 0} {$u < $br} {incr u} {
			dict set chn $r:$u 0
		}
	}
	
	dict set chn f:0 0
	for {set r 0} {$r < $k} {incr r} {
		set br [srcs::c_matr_get .b 0 $r int]
		set maxuse [dict get $peak $r]
		set br $maxuse

 		foreach {j sj} $s {
			set dj [srcs::c_matr_get .d 0 $j real]
 			set qj [srcs::c_matr_get .q $j $r int]
			dict set chn u:$r:$j [list]
			
			set wj $qj
			for {set u 0} {$u < $br && $wj > 0} {incr u} {
				set i [dict get $chn $r:$u]
				if {[dict get $chn f:$i] > $sj} {
 					continue
				}
				dict set chn $r:$u $j
				dict lappend chn u:$r:$j $u
				incr wj -1
  			}
			
			dict set chn f:$j [expr $sj + $dj]
			
		}
	}
	
	return $chn
}

# -- scale
proc scale {sc p} {
	set inmin [dict get $sc inmin]
	set inmax [dict get $sc inmax]
 	set outmin [dict get $sc outmin]
	set outmax [dict get $sc outmax]
	
	if {$p < $inmin || $p > $inmax} {
		error "out of range"
	}
	
	set p [expr 1.0*$p]
	
	set w [expr ($p - $inmin)/($inmax - $inmin)]
	set v [expr $outmin + $w*($outmax - $outmin)]
		
	return $v
}

# -- draw
proc draw {cv xscale yscales dir} {
	global n k conf maxX maxY id numlines lines peaks
	global COLOR0 COLOR1 COLOR2 COLOR3 COLOR4
	global flags
	global trqmaxY trqyscale trqxscale trqfn trqfncolors
	global temps
	
	if {$dir == "next"} {
		set id [expr min($id+1, $numlines)]
	} else {
		set id [expr max($id-1, 0)]
	}
	
	set s [lindex $lines $id]
	set peak [lindex $peaks $id]
 	set m [lindex $s 0]
	set z [lindex $s end]
	set T [lindex $s end-1]
	set s [lrange $s 1 end-2]
	set n [llength $s]
 
	# sort schedule
	proc cmp {a b} {
		lassign $a i si vi
		lassign $b j sj vj
		
		if {$si < $sj} {
			return -1
		} 
		if {$si > $sj} {
			return +1
		}
		if {$vi == 1 && $vj == 0} {
			return -1
		}
		return [expr $i - $j]
	}
	
	set s' [list]
	foreach {j sj} $s {
		lappend s' [list $j $sj [dict get $flags seen:$j]]
	}
 	set s' [lsort -command cmp -increasing ${s'}]
	set s [list]
	foreach jsj ${s'} {
		lassign $jsj j sj
		lappend s $j $sj
	}
	
	# indicate inserted task
	.l1 configure -text [format "new task: %-3d" $m] -font Helvetica,20 -fg red
	
	# mark inserted task as seen
	dict set flags seen:$m 1
	 	
	# initialize chains
	set chn [chaining $s $peak]
  	
 	# clear canvas
 	$cv delete "all"	
 	
	# draw chains
	for {set r 0} {$r < $k} {incr r} {
		set yscale [dict get $yscales $r]
 		
		foreach {j sj} $s {
 		
			set dj [srcs::c_matr_get .d 0 $j real]
 			set qj [srcs::c_matr_get .q $j $r int]	
 			set fj [dict get $chn f:$j]
 			# if {$dj == 0} {continue}
 			
 			set outline "black"
 			if [dict get $flags seen:$j] {
 				set color $COLOR1
 			} else {
 				set color $COLOR2
 				set outline "white"
 			}
  			if {$m == $j} {
 				set color $COLOR3
 			}
 						
			if {[srcs::c_plan_get_edge .plan.tc $m $j]} {
				set color $COLOR4
			}
			
			# override color by temperature settings
 			set color [dict get $temps $j]
 			set outline "black"
 			
 			set rx1 [scale $xscale $sj]
 			set rx2 [scale $xscale $fj]
 			
 			foreach u [dict get $chn u:$r:$j] {
 				set ry1 [expr $maxY - [scale $yscale $u]]
 				set ry2 [expr $maxY - [scale $yscale [expr $u+1]]]
 				
 				# puts "$j: ($rx1 $ry1), ($rx2, $ry2), $color "
 				$cv create rect $rx1 $ry1 $rx2 $ry2 -outline black -fill $color -outline $outline
 				
 				set tx [scale $xscale [expr $sj + 0.5*$dj]]
 				set ty [expr $maxY - [scale $yscale [expr $u + 0.5]]]
 				$cv create text $tx $ty -text $j 
 			}
		}
	}
	
	# plot torque
 	.top.cv delete "all"	
	
	set trqx [scale $trqxscale $id]
	set trqy [expr $trqmaxY - [scale $trqyscale $T]]
  	lappend trqfn $trqx
	lappend trqfn $trqy
	lappend trqfncolors [dict get $temps $m]
	
	set trqfnpoint 0
	foreach {x y} [lrange $trqfn 0 [expr ($id+1)*2 - 1]] {
		.top.cv create text $x $y -text "o" -fill [lindex $trqfncolors $trqfnpoint]
		incr trqfnpoint
	}

	if {$id > 0} {
		.top.cv create line {*}[lrange $trqfn 0 [expr ($id+1)*2 - 1]] 
	}
	
	puts "result: $z"
}

# -- main, instance related
source srcs.tcl
source srcs_sgs.tcl
source s-rcpsp/rho/prelude.tcl
source s-rcpsp/rho/lib.tcl

# -- main, ui-related
frame .f1
frame .f2
pack .f1 .f2

set maxX 1920
set maxY 980

canvas .cv -width $maxX -height $maxY -bg $COLOR0
pack .cv -in .f1


# calculate LFTs
srcs::c_matr_new .plan.est 1 $n
srcs::c_matr_new .plan.lft 1 $n
srcs::c_mcarlo_est .plan.est .plan .plan.order .d 1
set sn [srcs::c_matr_get .plan.est 0 [expr $n-1] real]
set dn [srcs::c_matr_get .d 0 [expr $n-1] real]
srcs::c_mcarlo_lft .plan.lft .plan .plan.order .d [expr $sn+$dn] 1

# convert LFTs to temperatures
set temps [dict create]
dict set temps max 0
dict set temps min [expr inf]
for {set i 0} {$i < $n} {incr i} {
	set t [srcs::c_matr_get .plan.lft 0 $i real]
	set t [expr int($t)]
	dict set temps max [expr max($t, [dict get $temps max])]
	dict set temps min [expr min($t, [dict get $temps min])]
}

set spread [expr [dict get $temps max]-[dict get $temps min]]
dict set temps colors [colorgradient $spread red yellow]

set tmin [dict get $temps min]
for {set i 0} {$i < $n} {incr i} {
	set t [srcs::c_matr_get .plan.lft 0 $i real]
	set t [expr min(int($t) - $tmin, $spread-1)]	
	set c [lindex [dict get $temps colors] $t]
	dict set temps $i $c
}

# read schedules from stdin
set lines [list]
set tmax 0
set trqmax 0
set trqmin [expr inf]
for {set numlines 0} {[gets stdin line] >= 0} {incr numlines} {
	lappend lines $line
	foreach {j sj} [lrange $line 1 end-2] {
		set dj [srcs::c_matr_get .d 0 $j real]
		set tmax [expr max($tmax, $sj + $dj)]
	}
	
	# determine max use per resource
	set peak [dict create]
	for {set r 0} {$r < $k} {incr r} {
		set max 0
 		foreach {j sj} [lrange $line 1 end-2] {
 			set qj [srcs::c_matr_get .q $j $r int]
 			set use 0
			foreach {i si} [lrange $line 1 end-2] {
				set di [srcs::c_matr_get .d 0 $i real]
				set fi [expr $si + $di]
 				set qi [srcs::c_matr_get .q $i $r int]

				if {$si <= $sj && $sj < $fi} {
					incr use $qi
				}
			}
			set max [expr max($max, $use)]
		}
		dict set peak $r $max
 	}
 	lappend peaks $peak
 	
 	set trq [lindex $line end-1]
 	set trqmax [expr max($trqmax, $trq)]
 	set trqmin [expr min($trqmin, $trq)]
 	
 	puts -nonewline stderr  [format "loading .. %d\r" $numlines]
}


# count maximum total resource use
set bsum 0
for {set r 0} {$r < $k} {incr r} {
	set maxuse 0
	foreach peak $peaks {
		set maxuse [expr max($maxuse, [dict get $peak $r])]
	}
	incr bsum $maxuse
}
# determine available estate for vertical drawings
# by reserving space for padding
set hasY [expr $maxY - $k*0.05*$maxY]

# fix yscales 
set yscales [dict create]
set offt [expr 0.05*$maxY]
for {set r 0} {$r < $k} {incr r} {
	set maxuse 0
	foreach peak $peaks {
		set maxuse [expr max($maxuse, [dict get $peak $r])]
	}
	set hr [expr ((1.0*$maxuse) / $bsum)*$hasY]
	
	set yscale [dict create]
	dict set yscale inmin 0
	dict set yscale inmax $maxuse
	dict set yscale outmin $offt
	dict set yscale outmax [expr $offt + $hr]
	
	set offt [expr [dict get $yscale outmax] + 0.05*$maxY]
	
	dict set yscales $r $yscale
}

# fix xscale
set xscale [dict create]

dict set xscale inmin 0
dict set xscale inmax $tmax
dict set xscale outmin [expr 0.05 * $maxX]
dict set xscale outmax [expr 0.95 * $maxX]

# assign flags to tasks
set flags [dict create]
for {set i 0} {$i < $n} {incr i} {
	dict set flags seen:$i 0
}

# build interface
button .b1 -text "Prev" -command { draw .cv $xscale $yscales "prev"}
button .b2 -text "Next" -command { draw .cv $xscale $yscales "next"}
button .b3 -text "Play" -command { 
	while {$id != [expr $numlines - 1]} {
		draw .cv $xscale $yscales "next"
		update idletasks
		puts "$id / $numlines"
	}
}
button .b4 -text "Rewind" -command {
	set id 0
	for {set i 0} {$i < $n} {incr i} {
		dict set flags seen:$i 0
	}
	draw .cv $xscale $yscales "next"
}

label .l1 -text "new task: "

pack .b1 -in .f2 -side left -padx 2
pack .b2 -in .f2 -side left -padx 2
pack .b3 -in .f2 -side left -padx 2
pack .b4 -in .f2 -side left -padx 2
pack .l1 -in .f2 -side left -padx 8

# torque plot window

set trqmaxY [expr $maxY * 0.3]
set trqmaxX $maxX

toplevel .top -width $maxX -height $trqmaxY
wm transient .top .
canvas .top.cv -width $maxX -height $trqmaxY -bg $COLOR0
pack .top.cv

# torque plot
set trqyscale [dict create]
dict set trqyscale inmin $trqmin
dict set trqyscale inmax $trqmax
dict set trqyscale outmin [expr $trqmaxY * 0.05]
dict set trqyscale outmax [expr $trqmaxY * 0.95]

set trqxscale [dict create]
dict set trqxscale inmin 0
dict set trqxscale inmax $numlines
dict set trqxscale outmin [expr 0.02 * $maxX]
dict set trqxscale outmax [expr 0.98 * $maxX]

set trqfn [list]
set trqfncolors [list]

# draw first schedule
set id -1
draw .cv $xscale $yscales "next"
