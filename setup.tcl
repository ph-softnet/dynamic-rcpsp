source lib.tcl

if {$tms == ""} {
	error "tms not set"
}

set conf [srcs::load_tms $tms]
set n [dict get $conf n]
set k [dict get $conf k]
incr k -1


# task durations
if {![info exists simnum]} {
	set simnum 1
}
srcs::c_matr_new .d $simnum $n
for {set i 0} {$i < $n} {incr i} {
	srcs::c_matr_set .d 0 $i real [dict get $conf $i:D:m1] 
}

srcs::plan_new .plan $conf
srcs::c_matr_new .plan.order 1 $n
if { [srcs::c_plan_order .plan.order .plan] == -1 } {
	error ".plan cyclic"
}

srcs::c_plan_new .plan.tc $n
srcs::c_plan_tc .plan.tc .plan .plan.order 

# initialize
set bconf [srcs::fbi::reverse_conf $conf .plan]
srcs::plan_new .bplan $bconf
srcs::c_matr_new .bplan.order 1 $n
if { [srcs::c_plan_order .bplan.order .bplan] == -1 } {
	error ".bplan cyclic"
}

# resource constraints
srcs::c_matr_new .q $n $k
srcs::c_matr_new .b 1 $k
srcs::qb_new .q .b $conf

# zero vector
srcs::c_matr_new .zero 1 $n

