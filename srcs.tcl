package require Tcl 8.5
package require math::statistics
package require struct::set

load ./libsrcs.so

proc load_tms_A {f} {
	seek $f 0
	set des [dict create]
	
 	dict set des labels [list]
	
	dict lappend des labels "s"
	dict set des 0:label "s"
	dict set des 0:D:m1 0
	dict set des source:id 0
	
	set n 1
		
	while {[gets $f txtline] >= 0} {
		set line [split [string trim $txtline]]
		
		if { [lindex $line 0] == "A" } {
			set i [lindex $line 1]
			set j [lindex $line 2]	
			set label "a${i}_${j}"
			dict lappend des labels $label
			dict set des $label:i $i
			dict set des $label:j $j
			dict set des $n:D:m1 [lindex $line 3]
			dict set des $label:id $n
			dict set des $n:label $label
			dict set des $label:txt [lindex $line 4]
			incr n
		}
	}

	dict set des n $n
	return $des
}

proc load_tms_P {f desvar} {
	upvar $desvar des
	seek $f 0
	dict set des edges [list]

	while {[gets $f txtline] >= 0} {
		set line [split [string trim $txtline]]

		if { [lindex $line 0] == "P" } {
			set label1 "a[lindex $line 1]_[lindex $line 2]"
			set label2 "a[lindex $line 3]_[lindex $line 4]"
			set a1 [dict get $des $label1:id]
			set a2 [dict get $des $label2:id]
			
			dict lappend des $a1:isuc $a2
			dict lappend des $a2:ipre $a1
			dict lappend des edges $a1 $a2
		}
	}
	
	#
	# link all sources with main source
	#
	
	for {set a 1} {$a < [dict get $des n]} {incr a} {
		if {![dict exists $des $a:ipre]} {
			dict lappend des $a:ipre 0
			dict lappend des 0:isuc $a
			dict lappend des edges 0 $a
		}

	}
	return $des
}

proc load_tms_R {f desvar} {
	upvar $desvar des
	seek $f 0
	dict set des rlabels [list]
	
	set k 0
	while {[gets $f txtline] >= 0} {
		set line [split [string trim $txtline]]

		if { [lindex $line 0] == "R" } {
			set rlabel [lindex $line 1]
			set cap [lindex $line 2]
			dict lappend des rlabels $rlabel
			dict set des r:$rlabel:id $k
			dict set des $k:cap $cap
			incr k
		}
	}
	dict set des k $k
	return $des
}

proc load_tms_Q {f desvar} {
	upvar $desvar des
	
	seek $f 0
	while {[gets $f txtline] >= 0} {
		set line [split [string trim $txtline]]

		if { [lindex $line 0] == "Q" } {
			set label "a[lindex $line 1]_[lindex $line 2]"
			set a1 [dict get $des $label:id]
			set rlabel [lindex $line 3]
			set r [dict get $des r:$rlabel:id]
			set req [lindex $line 4]
			dict set des $a1:req:$r $req
		}
	}

	# 
	# set zero requirements
	#
	
	for {set a 0} {$a < [dict get $des n]} {incr a} {		
		for {set r 0} {$r < [dict get $des k]} {incr r} {
			if {![dict exists $des $a:req:$r]} {
				dict set des $a:req:$r 0
			}
		}
	}

	return $des
}

proc load_tms_U {f desvar} {
	upvar $desvar des
	
	seek $f 0
	while {[gets $f txtline] >= 0} {
		set line [split [string trim $txtline]]

		if { [lindex $line 0] == "U" } {
			dict set des ub [lindex $line 1]
			break
		}
	}

	return $des
}

proc load_tms_s {f desvar} {
	upvar $desvar des
	
	seek $f 0
	while {[gets $f txtline] >= 0} {
		set line [split [string trim $txtline]]

		if { [lindex $line 0] == "s" } {
			set i [lindex $line 1]
			set j [lindex $line 2]
			set label "a${i}_${j}"
			
			set a1 [dict get $des $label:id]
			set t [lindex $line 3]
		
			dict set des $a1:s $t
			lappend atseq [list $a1 $t]
		}
	}
	
	dict set des 0:s 0
	set estaseq [list 0]
	
	foreach at [lsort -integer -index 1 $atseq] {
		lassign $at a t
		lappend estaseq $a
	}
	
	dict set des estaseq $estaseq

	return $des
}

# make all non-consumers use a big resource

proc srcs::make_tms_RQ {confvar} {
	upvar $confvar conf
	
	set n [dict get $conf n]
	set k [dict get $conf k]
	
	dict set conf $k:cap 0
	
	for {set i 0} {$i < $n} {incr i} {
		dict set conf $i:req:$k 0
	
		set rq 0
		for {set r 0} {$r < $k} {incr r} {
			if {[dict get $conf $i:req:$r]} {
				set rq 1
				break
			}
		}
		
		if {$rq == 0} {
			dict incr conf $k:cap
			dict set conf $i:req:$k 1
		}
	}
	
	dict incr conf k
}

proc srcs::load_tms {fnam0 {mode "none"}} {
	if {$mode != "none"} {
		set fnam [exec mktemp]
		exec ./util/tms_moded $fnam0 $mode > $fnam
	} else {
		set fnam $fnam0
	}
	set f [open $fnam r]
	
	set conf [load_tms_A $f]
	load_tms_P $f conf
	load_tms_R $f conf
	load_tms_Q $f conf
	load_tms_U $f conf
	make_tms_RQ conf
	dict set conf path $fnam
	
	if {$mode != "none"} {
		load_tms_s $f conf
		exec rm -f $fnam
	}
	
	close $f
	
	return $conf
}


proc srcs::plan_print_dot {sol des {f stdout}} {
	puts $f "strict digraph G \{"
	puts $f "rankdir = LR"
	puts $f "bgcolor = \"white\""
	puts $f "root = a0"
	puts $f "node \[shape=circle,width=0.5,fixedsize=true,color=white,fontcolor=black\];"
	
	set n [c_plan_get_n $sol]
	for {set a1 0} {$a1 < $n} {incr a1} {
		for {set a2 0} {$a2 < $n} {incr a2} {
			set edge [c_plan_get_edge $sol $a1 $a2]
			if {$des != ""} {
				set l1 [dict get $des $a1:label]
				set l2 [dict get $des $a2:label]
			} else {
				set l1 ""
				set l2 ""
			}
			if {$edge == 1} {
				#puts $f "\t\"$l1/$a1\" -> \"$l2/$a2\""
				puts $f "\t\"$a1\" -> \"$a2\""
			
			} elseif {$edge > 1} {
				#puts $f "\t\"$l1/$a1\" -> \"$l2/$a2\" \[color=\"#ff00ff\"\]"
				puts $f "\t\"$a1\" -> \"$a2\" \[color=\"#ff00ff\"\]"
			}
		}
	}
	puts $f "\}"
}


proc srcs::plan_new {plan conf} {
	set n		[dict get $conf n]
	set edges	[dict get $conf edges]
	
	srcs::c_plan_new $plan $n
	foreach {a1 a2} $edges {
		srcs::c_plan_set_edge $plan $a1 $a2 1
	}
}

proc srcs::taskfmt {prj id mean txt} {
	return "A $prj $id $mean \"$txt\""
}

proc srcs::edgefmt {p1 t1 p2 t2} {
	return "P $p1 $t1 $p2 $t2"
}

proc srcs::resfmt {res cap txt} {
	return "R $res $cap \"$txt\""
}

proc srcs::reqfmt {prj tsk res req} {
	return "Q $prj $tsk $res $req"
}

proc srcs::qb_new {q b conf} {
	set n [dict get $conf n]
	set k [dict get $conf k]

	for {set r 0} {$r < $k} {incr r} {
		srcs::c_matr_set $b 0 $r int [dict get $conf $r:cap]
		for {set i 0} {$i < $n} {incr i} {
			srcs::c_matr_set $q $i $r int [dict get $conf $i:req:$r]
		}
	}
}

proc srcs::mfslist {q b plan plantc conf} {
	set n [dict get $conf n]
	# compute the complement of the transitive closure
	srcs::c_matr_new $plan.tcnot $n $n
	for {set i 0} {$i < $n} {incr i} {
		for {set j 0} {$j < $n} {incr j} {
			if {![srcs::c_plan_get_edge $plantc $i $j] && ![srcs::c_plan_get_edge $plantc $j $i] \
				&& $i != $j} {
				srcs::c_matr_set $plan.tcnot $i $j int 1
			} else {
				srcs::c_matr_set $plan.tcnot $i $j int 0
			}
		}
	}
	
	# make ft
	set ret [srcs::c_ft .ft .q .b $plan.tcnot]
	srcs::c_matr_free $plan.tcnot
	srcs::c_ptr_del $plan.tcnot
	return $ret
}

proc srcs::mfsconf {mfslist mfsconfptr conf} {
	upvar $mfsconfptr mfsconf
 	 
	set k [expr [dict get $conf k]-1]
	array set mfsconf {}

 	set mfsconf(maxcvrnum) 0
	set mfsconf(maxrcvrnum) 0
	set mfsconf(sbsnum) 0
	set mfsconf(mfsnum) 0

	foreach mfs $mfslist {
 		lappend mfsconf(mfs) [list $mfs [llength $mfs]]
	}
 	set mfsconf(mfs) [lsort -increasing -integer -index 1 $mfsconf(mfs)]

	foreach elem $mfsconf(mfs) {
		lassign $elem mfs mfslen
	
		set mfsconf(rcvr:$mfsconf(mfsnum)) [list]
		for {set k1 0} {$k1 < [llength $mfs]} {incr k1} {
			set i [lindex $mfs $k1]
		
			for {set k2 [expr $k1+1]} {$k2 < [llength $mfs]} {incr k2} {
				set j [lindex $mfs $k2]
			
				if {![info exists mfsconf(id:$i,$j)]} {
					set mfsconf(id:$i,$j) [set sbsid $mfsconf(sbsnum)]
					set mfsconf(i:$sbsid) $i
					set mfsconf(j:$sbsid) $j
					set mfsconf(cvr:$sbsid) [list $mfsconf(mfsnum)]
					incr mfsconf(sbsnum)				
				} else {
					set sbsid $mfsconf(id:$i,$j)
					struct::set include mfsconf(cvr:$sbsid) $mfsconf(mfsnum)
				}
			
				lappend mfsconf(rcvr:$mfsconf(mfsnum)) $sbsid
				set rcvrnum [llength $mfsconf(rcvr:$mfsconf(mfsnum))]
				set mfsconf(maxrcvrnum) [expr max($mfsconf(maxrcvrnum),$rcvrnum)]

				set cvrnum [llength $mfsconf(cvr:$sbsid)]
				set mfsconf(maxcvrnum) [expr max($mfsconf(maxcvrnum),$cvrnum)]
			}
		}
		incr mfsconf(mfsnum)
	}
	
}

proc srcs::mfs_new {mfs conf mfsconfptr} {
	upvar $mfsconfptr mfsconf
	
 	# 'sbs' stands for subset
 	# 'cvr' stands for cover
 	
	set n [dict get $conf n]
	
	srcs::c_matr_new $mfs.sbs $mfsconf(sbsnum) 4
	srcs::c_matr_new $mfs.cvr $mfsconf(sbsnum) $mfsconf(maxcvrnum)
	srcs::c_matr_new $mfs.map $n $n
	srcs::c_matr_new $mfs.mfs $mfsconf(mfsnum) [expr {$mfsconf(maxrcvrnum)+1}] 
 
	# fill-in sbs & cvr matrices & map 

	for {set i 0} {$i < $n} {incr i} {
		for {set j 0} {$j < $n} {incr j} {
			srcs::c_matr_set $mfs.map $i $j int -1
		}
	}

	for {set sbsid 0} {$sbsid < $mfsconf(sbsnum)} {incr sbsid} {
 		srcs::c_matr_set $mfs.sbs $sbsid 0 int [set i $mfsconf(i:$sbsid)]
		srcs::c_matr_set $mfs.sbs $sbsid 1 int [set j $mfsconf(j:$sbsid)]
		srcs::c_matr_set $mfs.sbs $sbsid 2 int [set cvrnum [llength $mfsconf(cvr:$sbsid)]]
		srcs::c_matr_set $mfs.sbs $sbsid 3 int [llength $mfsconf(cvr:$sbsid)]
 	
		srcs::c_matr_set $mfs.map $i $j int $sbsid
		srcs::c_matr_set $mfs.map $j $i int $sbsid
	
		for {set cvrid 0} {$cvrid < $cvrnum} {incr cvrid} {
			srcs::c_matr_set $mfs.cvr $sbsid $cvrid int \
			[lindex $mfsconf(cvr:$sbsid) $cvrid]
		}
	}

	for {set mfsid 0} {$mfsid < $mfsconf(mfsnum)} {incr mfsid} {
		srcs::c_matr_set $mfs.mfs $mfsid 0 int [llength [set rcvr $mfsconf(rcvr:$mfsid)]]
		set rcvrid 1
		foreach sbsid $rcvr {
			srcs::c_matr_set $mfs.mfs $mfsid $rcvrid int $sbsid
			incr rcvrid
		}
	}
}

#
# build a bimodal distribution
# description in "user-form" of the task durations,
# given only the task duration mean values
#

proc srcs::bimod_in_uf {conf} {
	set dst [list]
	set n [dict get $conf n]
	
	set m1max 0
	for {set i 1} {$i < $n} {incr i} {
		set m1 [dict get $conf $i:D:m1]
		set m1max [expr max($m1max,$m1)]
	}
	
	for {set i 1} {$i <$n} {incr i} {
		set m1 [dict get $conf $i:D:m1]
		
		if {[expr $i % 9] <= 5} {
			set q [expr 0.5 + 0.1*($i % 9)]
		} else {
			set q [expr 0.5 - 0.1*($i % 9)]
		}
		set q [expr min(0.7, max(0.1, $q))]
		
		if {[expr $m1/$m1max] >= 0.8} {
			set q [expr min(0.3, $q)]
		}
		if {[expr $m1/$m1max] <= 0.3} {
			set q [expr max(0.6, $q)]
		}
		set t1 1
		set t2 [expr 5*$m1]
		set t3 [expr int(1.5*$t2)]
		
		lappend dst $i
		lappend dst [list $t1 $t2 $t3 $q]
	}
	return $dst
}


#
# convert the CDF from
# bimodal distribution to list-form and
# from list-form to sum-form
#

proc bimod_to_lf {dst} {
	lassign $dst t1 t2 t3 q
	set p1 [expr (1-$q)/$t1]
	set p2 [expr $q/($t3-$t2)]
	
	set lf [list]
	lappend lf [list $p1 0]
	lappend lf [list 0.0 $t1]
	lappend lf [list $p2 $t2]
	lappend lf [list 0.0 $t3]
	
	return $lf
}

proc lf_to_sf {lf} {
	set sf [list]
	set lf0 [lindex $lf 0]
	lappend sf [list [lindex $lf0 0] 0]
	
	for {set j 1} {$j < [llength $lf]} {incr j} {
		lassign [lindex $lf $j] lfj1 lfj2
		lassign [lindex $lf [expr $j-1]] lfi1 lfi2
		
		set ai [expr $lfj1-$lfi1]
		set ti $lfj2
		
		lappend sf [list $ai $ti]
	}
	
	return $sf
}

#
# describe the CDF of task durations as a sum of PWCs such that
# it can be used as input to PWL SSTA
#

proc srcs::bimod_pwc_sf {Dobj n bimod} {

	#set n [dict get $conf n]
	
	srcs::c_matr_new $Dobj [expr 1+2*4] $n

	foreach {i dst} $bimod {
		set lf [bimod_to_lf $dst]
 		set sf [lf_to_sf $lf]
 		
 		#print Mathematica description
 		#puts -nonewline "Duf\[$i\]={"
 		#for {set x 0} {$x < [llength $dst]} {incr x} {
 		#	puts -nonewline [lindex $dst $x]
 		#	if {$x < [llength $dst]-1} {
 		#		puts -nonewline ", "
 		#	}
 		#}
 		#puts "};"
 	
 		srcs::c_matr_set $Dobj 0 $i int 4
 		for {set r 0} {$r < 4} {incr r} {
 			lassign [lindex $sf $r] v t
 			srcs::c_matr_set $Dobj [expr 1+2*$r] $i real $v
  			srcs::c_matr_set $Dobj [expr 1+2*$r+1] $i int $t
 		}
	}	
}

#
# sample durations according to a user-form
# description of bimodal task durations
#

proc srcs::bimod_sample {dobj simmax n bimod} {

	foreach {i dst} $bimod {
		#puts -nonewline "$i: $dst: "
		
		for {set m 0} {$m < $simmax} {incr m} {
			
			lassign $dst t1 t2 t3 q
			
			set w [math::random]
		
			if {[math::random] <= $q} {
				set t [expr $t2+$w*($t3-$t2)]
			} else {
				set t [expr 0+$w*($t1-0)]
			}
			set t [expr [format "%.1f " $t]]
			
			if {$i == 0} {set t 0}
			if {$i == [expr $n-1]} {set t 0}
			
			#puts -nonewline [format "%-2.1f\t" $t]
			
			srcs::c_matr_set $dobj $m $i real $t
		}
		#puts ""
	}
}

#
# call heuristic solver on instance
#
proc srcs::frits {path {numsamp 500}} {
	set err [catch \
		{set out [exec java in5000.sgs.Solver $path $numsamp]} errmsg]
 	if $err {error $errmsg}
	
	set s [list 0]
	foreach {i si} $out {
		lappend s $si
	}
 	
 	return $s
}

proc srcs::matr_set_row {mobj row data {type real}} {
	for {set i 0} {$i < [llength $data]} {incr i} {
		srcs::c_matr_set $mobj $row $i $type [lindex $data $i]
	}
}

proc srcs::matr_get_row {mobj row {type real}} {
	set ret [list]
	for {set col 0} {$col < [srcs::c_matr_get_cols $mobj]} {incr col} {
		lappend ret [srcs::c_matr_get $mobj $row $col $type]
	}
	return $ret
}

proc srcs::matr_copy_row {mobj1 row1 mobj2 row2 {type real}} {
	set numcol [srcs::c_matr_get_cols $mobj1]
	if {$numcol != [srcs::c_matr_get_cols $mobj2]} {
		error "$mobj1 incompatible with $mobj2"
	}
	
	for {set col 0} {$col < $numcol} {incr col} {
		srcs::c_matr_set $mobj1 $row1 $col $type \
		[srcs::c_matr_get $mobj2 $row2 $col $type]
	}
}

proc srcs::matr_get_col {mobj col {type real}} {
	set ret [list]
	for {set row 0} {$row < [srcs::c_matr_get_rows $mobj]} {incr row} {
		lappend ret [srcs::c_matr_get $mobj $row $col $type]
	}
	return $ret
}

proc srcs::sortlist {l {mode pairs}} {
	set pairs [list]
	set n [llength $l]
	for {set i 0} {$i < $n} {incr i} {
		lappend pairs [list $i [lindex $l $i]]
	}
	set pairs [lsort -real -index 1 -increasing $pairs]

	if {$mode == "pairs"} {
		return $pairs
	}

	if {$mode == "values"} {
		set values [list]
		foreach pair $pairs {
			lassign $pair i val
			lappend values $val
		}
		return $values
	}

	if {$mode == "indices"} {
		set indices [list]
		foreach pair $pairs {
			lassign $pair i val
			lappend indices $i
		}
		return $indices
	}

	error "unknown mode: $mode"
}
