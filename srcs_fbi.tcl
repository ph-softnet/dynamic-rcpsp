# -- fbi.tcl
#
#	utilities for Forward Backward Improvement (FBI)
#

namespace eval srcs::fbi {
	namespace export reverse_conf decreasing_lft_sort
}

# -- reverse precedence constraints
proc srcs::fbi::reverse_conf {conf plan} {
	set n [dict get $conf n]
	set bconf $conf
	dict set bconf edges [list]
	set numpre [dict create]
	
	for {set i 0} {$i < $n} {incr i} {
		dict set numpre $i 0
		dict set bconf $i:ipre [list]
		dict set bconf $i:isuc [list]
	}
	
	# reverse (i,j) in {1, .., n}^2
	for {set i 1} {$i < $n} {incr i} {
		dict set numpre $i 0
		for {set j 1} {$j < $n} {incr j} {
			if [srcs::c_plan_get_edge $plan $i $j] {
				dict incr numpre $i
				dict lappend bconf edges $j $i
				dict lappend bconf $i:ipre $j
				dict lappend bconf $j:isuc $i
			}
		}
	}
	# point 0 to those with no predecessors
	for {set i 1} {$i < $n} {incr i} {
		if {[dict get $numpre $i] == 0} {
			dict lappend bconf edges 0 $i
			dict lappend bconf 0:isuc $i
		}
	}
	
	return $bconf
}

# -- return a sequencing by decreasing lft according to s[]
proc srcs::fbi::desc_lft_sort {d s} {
	set n [llength $d]
	if {$n != [llength $s]} {
		error "d() does not match s()"
	}
	
	set f [list]
	for {set i 0} {$i < $n} {incr i} {
		set si [lindex $s $i]
		set di [lindex $d $i]
		set fi [expr $si + $di]
		lappend f [expr -$fi]
	}
	return [srcs::sortlist $f indices]
}
